/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/15 17:04:23 by hskikdi           #+#    #+#             */
/*   Updated: 2021/08/15 17:04:28 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H
# include "libft.h"
# include <unistd.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdlib.h>

char	*get_env(char *s, char **tab);
int		ft_error(char *str);
int		ft_open_file(char *file, int mode);
char	*create_path(char *s1, char *s2);
char	*find_exec(char *name, char **env);
int		ft_access(char *path);
int		ft_error(char *str);
char	*create_path(char *s1, char *s2);
#endif
