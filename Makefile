
OBJ=$(addsuffix .o, src/utils src/pipex)

NAME=pipex

FLAG=-Wall -Werror -Wextra

all: $(NAME)

$(NAME): $(OBJ)
	make -C mini_lib
	gcc $(FLAG) $^ -o $@ -Lmini_lib -lft

%.o: %.c
	gcc $(FLAG) -c -o $@ $< -Iincludes

clean:
	make clean -C mini_lib
	rm -rf $(OBJ)

fclean: clean
	rm -f $(NAME)
	make fclean -C mini_lib

re: fclean all
