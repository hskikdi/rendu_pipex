/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/15 16:54:30 by hskikdi           #+#    #+#             */
/*   Updated: 2021/08/17 20:24:23 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "pipex.h"

int	ft_open_file(char *file, int mode)
{
	int		fd;

	if (!mode)
		fd = open(file, O_RDONLY);
	else
		fd = open(file, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU | S_IRWXG);
	if (!ft_access(file))
		return (-1);
	return (fd);
}

char	*find_exec(char *name, char **env)
{
	char	**paths;
	char	*tmp;
	char	**to_free;

	if (*name == '.')
		return (ft_strdup(name));
	tmp = get_env("PATH", env);
	paths = ft_split(tmp, ':');
	to_free = paths;
	ft_strdel(&tmp);
	while (paths && *paths)
	{
		tmp = create_path(*paths, name);
		if (tmp && access(tmp, F_OK) != -1 && access(tmp, R_OK) != -1)
		{
			ft_arrfree(to_free);
			return (tmp);
		}
		ft_strdel(&tmp);
		paths++;
	}
	ft_arrfree(to_free);
	return (ft_strdup(""));
}

void	ft_exec_child(char *infile, char *cmd, char **env, int *fd)
{
	int		fd_infile;
	char	**params;
	char	*exec;

	fd_infile = ft_open_file(infile, 0);
	if (fd_infile == -1)
		ft_error(infile);
	close(fd[0]);
	dup2(fd_infile, STDIN_FILENO);
	dup2(fd[1], STDOUT_FILENO);
	params = ft_split(cmd, ' ');
	exec = find_exec(*params, env);
	execve(exec, params, env);
	ft_putstr_fd("pipex commande not found: ", 2);
	ft_putendl_fd(*params, 2);
	ft_strdel(&exec);
	ft_arrfree(params);
	exit(0);
}

void	ft_exec_father(char *cmd, char *outfile, char **env, int *fd)
{
	int		fd_outfile;
	char	**params;
	char	*exec;

	fd_outfile = ft_open_file(outfile, 1);
	if (fd_outfile == -1)
		ft_error(outfile);
	close(fd[1]);
	dup2(fd[0], STDIN_FILENO);
	close(fd[0]);
	dup2(fd_outfile, STDOUT_FILENO);
	params = ft_split(cmd, ' ');
	exec = find_exec(*params, env);
	execve(exec, params, env);
	ft_putstr_fd("pipex commande not found: ", 2);
	ft_putendl_fd(*params, 2);
	ft_strdel(&exec);
	ft_arrfree(params);
	exit(0);
}

int	main(int ac, char **av, char **env)
{
	int		child;
	int		fd[2];

	if (ac != 5)
		ft_error("usage pipex : 4 arguments expected");
	if (pipe(fd) == -1)
		ft_error("pipe error");
	child = fork();
	if (child < 0)
		ft_error("fork error");
	if (!child)
	{
		ft_exec_child(av[1], av[2], env, fd);
	}
	else
	{
		ft_exec_father(av[3], av[4], env, fd);
	}
	return (0);
}
