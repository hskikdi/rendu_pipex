/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hskikdi <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/15 17:00:14 by hskikdi           #+#    #+#             */
/*   Updated: 2021/08/15 17:00:20 by hskikdi          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"

char	*get_env(char *name, char **env)
{
	int			lenght;
	int			i;

	i = 0;
	lenght = ft_strlen(name);
	while (env[i])
	{
		if (!ft_strncmp(name, env[i], lenght) && env[i][lenght] == '=')
			return (ft_strdup(env[i] + lenght + 1));
		else
			i++;
	}
	return (ft_strdup(""));
}

int	ft_access(char *path)
{
	if (access(path, F_OK) == -1)
		ft_putstr_fd("open: no such file or directory: ", 2);
	else if (access(path, R_OK) == -1)
		ft_putstr_fd("open: permission denied: ", 2);
	else if (access(path, W_OK) == -1)
		ft_putstr_fd("open: permission denied: ", 2);
	else
		return (1);
	return (0);
}

char	*create_path(char *s1, char *s2)
{
	char	*path;
	char	*tmp;

	tmp = ft_strjoin(s1, "/");
	if (!tmp)
		ft_error("malloc error");
	path = ft_strjoin(tmp, s2);
	if (!path)
		ft_error("malloc error");
	ft_strdel(&tmp);
	return (path);
}

int	ft_error(char *str)
{
	int	len;

	len = 0;
	while (str[len])
		len++;
	write(2, str, len);
	write(2, "\n", 1);
	exit(0);
}
